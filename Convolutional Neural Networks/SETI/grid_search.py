from functions import *
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import GridSearchCV
import numpy as np
from random import sample

classes=('brightpixel', 'narrowband', 'narrowbanddrd','noise', 'squarepulsednarrowband','squiggle',
         'squigglesquarepulsednarrowband')

val_gen=val_generator(classes)

lim=100
img_dim=197

x, y=[], []
for i in range(0, lim):
    x.append(val_gen[i][0])
    y.append(val_gen[i][1])

del val_gen

x, y=np.array(x), np.array(y)
x, y=np.reshape(x, (lim, 197, 197, 3)), np.reshape(y, (lim, 7))

build_fn=create_model

model=KerasClassifier(build_fn=build_fn, verbose=2)

#define params for gridsearch
#batch_size=[10, 20, 40, 60, 80, 100]
#epochs=[10, 30, 50, 70, 100]
#optimizer=['SGD', 'RMSprop', 'Adagrad', 'Adadelta', 'Adam', 'Adamax', 'Nadam']
#learn_rate=[0.001, 0.01, 0.02, 0.03]
#momentum=[0., 0.2, 0.4, 0.6, 0.8, 0.9]
#init_mode=['uniform', 'lecun_uniform', 'normal', 'zero', 'glorot_normal', 'glorot_uniform',
#	   'he_normal', 'he_uniform']
#activation=['softmax', 'softplus', 'softsign', 'relu', 'tanh', 'sigmoid', 'hard_sigmoid', 'linear']
#dropout=[0., 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
neurons=[img_dim, int(1.25*img_dim), int(1.5*img_dim), int(1.75*img_dim), 2*img_dim]  

param_grid=dict(neurons=neurons)

grid=GridSearchCV(estimator=model, param_grid=param_grid, n_jobs=6, cv=3)

grid_result=grid.fit(x, y)

#Summarize results
print('Best: %f using %s' % (grid_result.best_score_, grid_result.best_params_))
means=grid_result.cv_results_['mean_test_score']
stds=grid_result.cv_results_['std_test_score']
params=grid_result.cv_results_['params']

for mean, stdev, param in zip(means, stds, params):
    print('%f(%f) with: %r' % (mean, stdev, param))
