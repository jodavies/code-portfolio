import numpy as np
seed=np.random.seed(42)
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Dropout, Flatten, MaxPool2D
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import RMSprop

def val_generator(classes, val_dir='valid/', img_dim=197):
    val_datagen=ImageDataGenerator()
    val_batch_size=1
    val_gen=val_datagen.flow_from_directory(val_dir,
                                            classes=classes,
                                            target_size=(img_dim, img_dim),
                                            batch_size=val_batch_size,
                                            class_mode='categorical',
                                            shuffle=True, seed=seed)
    return val_gen

def generate_data(classes, train_dir='train/', test_dir='test/', val_dir='valid/', img_dim=197):
    train_datagen=ImageDataGenerator(rotation_range=180, horizontal_flip=True, vertical_flip=True,
                                 fill_mode='reflect')
    #Validation data should not be augmented. We augment the training data to increase training accuracy
    val_datagen=ImageDataGenerator()
    test_datagen=ImageDataGenerator()
    #ImageDataGenerator is a class object and will generate tensor versions of the image data and can augment them in real time as they are loaded in batches

    train_batch_size=64 #we want to get an average error
    val_batch_size=64
    test_batch_size=1 #want to make sure we are testing one at a time 

    train_gen=train_datagen.flow_from_directory(train_dir,
                                            classes=classes,
                                            target_size=(img_dim, img_dim),
                                            batch_size=train_batch_size,
                                            class_mode='categorical',
                                            shuffle=True, seed=seed)
    #flow_from_directory will allow us to load images using the directories we have defined above
    #We let the function know the classes (different sub-directory names), target size of the images, how
    #many batches we wish to do at one time, whether we have categorical data or not and whether to shuffle
    #the images to increase the random selection element

    val_gen=val_datagen.flow_from_directory(val_dir,
                                            classes=classes,
                                            target_size=(img_dim, img_dim),
                                            batch_size=val_batch_size,
                                            class_mode='categorical',
                                            shuffle=True, seed=seed)
    test_gen=train_datagen.flow_from_directory(train_dir,
                                            classes=classes,
                                            target_size=(img_dim, img_dim),
                                            batch_size=test_batch_size,
                                            class_mode='categorical',
                                            shuffle=False, seed=seed)
    #There's no need to shuffle here because it won't affect our generalization error score

    return train_gen, val_gen, test_gen

def create_model(img_dim=197, optimizer='Adam', init_mode='he_normal', activation='relu', 
		 dropout=0.2, neurons=344):
    input_shape=(img_dim, img_dim, 3)
    #Creating our model
    model=Sequential()

    model.add(Conv2D(filters=32, kernel_size=(3), strides=(2,2), padding='same', activation=activation,
                 input_shape=input_shape, kernel_initializer=init_mode))
    model.add(Conv2D(filters=32, kernel_size=(3), strides=(2,2), padding='same', activation=activation))
    model.add(Conv2D(filters=64, kernel_size=(3), strides=(2,2), padding='same', activation=activation))
    model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))
    model.add(Conv2D(filters=96, kernel_size=(3), padding='same', activation=activation))
    model.add(Conv2D(filters=96, kernel_size=(3), padding='same', activation=activation))
    model.add(Conv2D(filters=128, kernel_size=(3), padding='same', activation=activation))
    model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))
    model.add(Conv2D(filters=160, kernel_size=(3), padding='same', activation=activation))
    model.add(Conv2D(filters=160, kernel_size=(3), padding='same', activation=activation))
    model.add(Conv2D(filters=192, kernel_size=(3), padding='same', activation=activation))
    model.add(MaxPool2D(pool_size=(2, 2), strides=(2,2)))

    print('-'*100)
    print('Convolutional layers (input layer) made')
    print('-'*100)

    model.add(Flatten())
    model.add(Dense(3*neurons, activation=activation, kernel_initializer=init_mode))
    model.add(Dense(3*neurons, activation=activation, kernel_initializer=init_mode))
    model.add(Dense(3*neurons, activation=activation, kernel_initializer=init_mode))
    model.add(Dense(3*neurons, activation=activation, kernel_initializer=init_mode))
    model.add(Dense(3*neurons, activation=activation, kernel_initializer=init_mode))
    model.add(Dropout(dropout))
    model.add(Dense(7, activation='softmax', kernel_initializer=init_mode))
    #We have 7 possible output categories here corresponding to our 7 output classes

    #Compile
    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
    
    return model

def create_model_inception():
	base = InceptionResNetV2(
  	weights = '../input/inceptionresnetv2/inception_resnet_v2_weights_tf_dim_ordering_tf_kernels_notop.h5',
  	include_top = False,
  	input_shape = (img_dim, img_dim, 3)
	)
	
	x = base.output
	x = Flatten(input_shape=base.output_shape[1:])(x)
	x = Dense(img_dim, activation="relu")(x)
	x = Dropout(0.2)(x)
	x = Dense(7, activation="softmax")(x)

	model = Model(inputs=base.input, outputs=x)

	for layer in model.layers:
   		layer.trainable = True

	model.compile(loss = "binary_crossentropy", optimizer = optimizers.rmsprop(lr=1e-4), metrics=["accuracy"])

	return model
