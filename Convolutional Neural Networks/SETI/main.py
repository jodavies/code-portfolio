import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

seed=np.random.seed(42)

from sklearn.metrics import confusion_matrix
from keras.callbacks import ReduceLROnPlateau
#mine
from functions import generate_data, create_model


#some useful constants
train_dir='train/'
test_dir='test/'
val_dir='valid/'
img_dim=197

classes=('brightpixel', 'narrowband', 'narrowbanddrd','noise', 'squarepulsednarrowband', 'squiggle',
         'squigglesquarepulsednarrowband')

train_gen, val_gen, test_gen=generate_data(classes)


print('-'*100)
print('Image data generated')
print('-'*100)

input_shape=(img_dim, img_dim, 3)

model=create_model()

lr_rate_reduction=ReduceLROnPlateau(monitor='val_accuracy', patience=3, verbose=1, factor=0.5,
                                    min_lr=0.00001)

print('-'*100)
print('Model compiled')
print('-'*100)

epochs=30
train_step=60
val_step=32

#Train
history=model.fit_generator(train_gen, steps_per_epoch=train_step, epochs=epochs,
        validation_data=val_gen, validation_steps=val_step, verbose=2, 
                            callbacks=[lr_rate_reduction])
#steps_per_epoch refers to how many batches of samples are taken from the generator

print('-'*100)
print('Model trained')
print('-'*100)

#plots
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('Model Accuracy')
plt.xlabel('Accuracy')
plt.ylabel('Epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

#Predict on the test set
preds=model.predict_generator(test_gen, steps=700, verbose=1) #700 referring to there being 700 images

df=pd.DataFrame(preds)
df['filename']=pd.Series(test_gen.filenames)
df['truth']=''
df['truth']=df['filename'].str.split('/', n=1, expand=True) #n limits number of splits in output

df['prediction_index'] = df[[0,1,2,3,4,5,6]].idxmax(axis=1)
df['prediction'] = ''
df['prediction'][df['prediction_index'] == 0] = 'noise'
df['prediction'][df['prediction_index'] == 1] = 'squiggle'
df['prediction'][df['prediction_index'] == 2] = 'narrowband'
df['prediction'][df['prediction_index'] == 3] = 'narrowbanddrd'
df['prediction'][df['prediction_index'] == 4] = 'squarepulsednarrowband'
df['prediction'][df['prediction_index'] == 5] = 'squigglesquarepulsednarrowband'
df['prediction'][df['prediction_index'] == 6] = 'brightpixel'

cm = confusion_matrix(df['truth'], df['prediction'])
print(cm)

accuracy = accuracy_score(df['truth'], df['prediction'])
print(accuracy)
