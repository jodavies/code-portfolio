import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
import matplotlib.image as mpimg
import seaborn as sns

seed=np.random.seed(42)

from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import itertools

from keras.utils.np_utils import to_categorical #this will handle the one-hot
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Dropout, Flatten, MaxPool2D
from keras.optimizers import RMSprop
#RMSprop keeps the moving average of the squared gradients in SGD for each weight, then will
#divide the gradient by the sqrt of the mean square
#This works similarly to adagrad (or ADAM) but is a bit faster generally

from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ReduceLROnPlateau #can use callbacks to get a view on internal states
#and statistics of the model during training

sns.set(style='white', palette='deep')

train=pd.read_csv('train.csv')
test=pd.read_csv('test.csv')

y_train=train['label']
x_train=train.drop(columns='label')
x_train=StandardScaler().fit_transform(x_train)

del train

#We now want to reshape the image from 28x28 pixels to 3D 28x28x1
#The extra dimension here corresponds to channels as this is required by keras. The MNIST set is
#greyscale so we only have 1. If they were RGB it would be 3 etc
x_train=x_train.reshape(-1, 28, 28, 1)
test=test.values.reshape(-1, 28, 28, 1)
#-1 indicates that we don't know the value of the batch param but would like it to be calculated

#Encode the labels (outputs) to one-hot vecs
y_train=to_categorical(y_train, num_classes=10)

x_train, x_val, y_train, y_val=train_test_split(x_train, y_train, test_size=0.2, random_state=seed)

#Create the model
model=Sequential()
model.add(Conv2D(filters=32, kernel_size=(5,5), padding='same', activation='relu',
                 input_shape=(28, 28, 1)))
#The filters are the number of outputs the layer will return ie how many 'interesting' bits of the
#image are we going to consider? How much are we going to decompose the image?
#The kernel size is how big the slider is over the pixels
#Padding will stop the dimensionality going down to quickly by padding out the image with 0s
#relu will add some non-linearity to the model

model.add(Conv2D(filters=32, kernel_size=(5, 5), padding='same', activation='relu'))
model.add(MaxPool2D(pool_size=(2,2)))
#Pooling acts as a downsampling filter in order to reduce computational cost (and overfitting)
#Ours will look at the two nearest neighbours and choose the highest value

model.add(Dropout(0.25))
#This is a regularization method that selects a percentage of nodes to be ignored for each
#training sample. This forces the network to learn features in a distributed way. This improves
#generalization and reduces overfitting

model.add(Conv2D(filters=64, kernel_size=(3, 3), padding='same', activation='relu'))
model.add(Conv2D(filters=64, kernel_size=(3, 3), padding='same', activation='relu'))
model.add(MaxPool2D(pool_size=(2, 2), strides=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(256, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(10, activation='softmax'))

#Define optimizer
optimizer=RMSprop(lr=0.001, rho=0.9, epsilon=1e-08, decay=0.0)
#Everything bar the learning rate is recommended to be left to these default values
#Learning rate can be tweaked and should be treated as another hyper-param to be tuned

#Compile the model
model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])

#To make the model converge faster and closer to the global minimum of the loss function, we can
#add an annealing method of the learning rate (annealing looks to find global min)
#The learning rate is how big the SGD steps are
learning_rate_reduction=ReduceLROnPlateau(monitor='val_accuracy', patience=3, verbose=1, factor=0.5,
                                          min_lr=0.00001)
#Here we have said that the LR should be reduced by half if it has not improved by 3 epochs

epochs=15
batch_size=86

#In order to get a higher accuracy and avoid overfitting we should expand our dataset artificially
#This is done by altering the training data with small transs. to reproduce variations occuring
#when someone is writing a digit
#This may not add a massive amount of accuracy but is still worth mentioning. If we don't want to
#include this, the next line would be:

#history = model.fit(X_train, Y_train, batch_size = batch_size, epochs = epochs, 
#          validation_data = (X_val, Y_val), verbose = 2)

datagen = ImageDataGenerator(
        featurewise_center=False,  # set input mean to 0 over the dataset
        samplewise_center=False,  # set each sample mean to 0
        featurewise_std_normalization=False,  # divide inputs by std of the dataset
        samplewise_std_normalization=False,  # divide each input by its std
        zca_whitening=False,  # apply ZCA whitening
        rotation_range=10,  # randomly rotate images in the range (degrees, 0 to 180)
        zoom_range = 0.1, # Randomly zoom image 
        width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
        height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
        horizontal_flip=False,  # randomly flip images
        vertical_flip=False)  # randomly flip images


datagen.fit(x_train)
#Anything not listed as false is considered here

#Fit the model
history=model.fit_generator(datagen.flow(x_train, y_train, batch_size=batch_size),
                            epochs=epochs, validation_data=(x_val, y_val),
                            verbose=2, steps_per_epoch=x_train.shape[0] // batch_size,
                            callbacks=[learning_rate_reduction])

# Plot the loss and accuracy curves for training and validation 
fig, ax = plt.subplots(2,1)
ax[0].plot(history.history['loss'], color='b', label="Training loss")
ax[0].plot(history.history['val_loss'], color='r', label="validation loss",axes =ax[0])
legend = ax[0].legend(loc='best', shadow=True)

ax[1].plot(history.history['accuracy'], color='b', label="Training accuracy")
ax[1].plot(history.history['val_accuracy'], color='r',label="Validation accuracy")
legend = ax[1].legend(loc='best', shadow=True)

#predict results
results=model.predict(test)

#select index with max prob
results=np.argmax(results, axis=1)
results=pd.Series(results, name='Label')

print(results)
