# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 13:44:35 2019

@author: Joe
"""

import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.impute import SimpleImputer
from keras.models import Sequential
from keras.layers import Dense

#create a data frame from our csv
df=pd.read_csv("WS_buildaccountdata.csv")

#Lowering the number of features to just include the integers
df1=df.drop(['tx_revision', 'nu_loc_total', 'nu_comment_total', 'nu_filesize_total', 'nu_cyclo_total', 'nu_dac_total', 
            'nu_fanout_total', 'nu_halstead_total', 'nu_nom_total', 'dt_submit_date', 'tm_submit_time', 'tx_state', 
           'tx_working_file_type'], axis=1)

#Get rid of any NaN values and replace them with the means of those features
imputer=SimpleImputer(missing_values=np.nan, strategy='mean')
for element in df1:
    df1[[element]]=imputer.fit_transform(df1[[element]])
    
#Collect our data from the dataframe and split into inputs (X) and output (y)
X=df1.iloc[:, 0:-1].values
Y=df1.iloc[:, -1].values

#Scaling the data to fit in the range of a tanh function (-1, 1)
scaler=MinMaxScaler(feature_range=(-1, 1))
X_scaled=scaler.fit_transform(X)

#Make the training and test data with a 80:20 split
X_train, X_test, y_train, y_test=train_test_split(X_scaled, Y, test_size=0.2)

#Make the classifier by constructing a 1-hidden-layer neural network
classifier=Sequential()

#Add the input layer and the the hidden layer. 12 inputs
classifier.add(Dense(12, kernel_initializer='normal', activation='relu'))
classifier.add(Dense(256, activation="relu"))
#Output layer 2 output classes
classifier.add(Dense(2, activation="softmax"))

#Compiling the neural network
classifier.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

#Fitting the model
classifier.fit(X_train, y_train, batch_size=1000, epochs=10)

#Test the classifier and ouput the accuracy and loss
score=classifier.evaluate(X_test, y_test, verbose=0)
score_array=['Loss', 'Accuracy']
print('(', score_array[0], ', ', score_array[1], ')', '= ', score)